## What?

Sample project for learning the basics of [angular2](https://angular.io/) framework. The project is based in the [tour of heroes tutorial](https://angular.io/docs/ts/latest/tutorial/). It consumes the REST API of the [products-core](https://gitlab.cern.ch/lurodrig/products-core) project. 

## Why?

Since 2012 I have been the developer and service manager of the [CERN Java Web Hosting Service](https://information-technology.web.cern.ch/services/java-web-hosting) and during 2016 I've seen that the number of applications that uses **angular2** has considerably increased. That's why I decided to have a look at this framework.

Also if you have attended the last [developers@CERN forum](https://indico.cern.ch/event/577155/overview) you had realized that more and more developers are adopting angular2.   

## Usage

Asumming that you have node and npm installed in your box. You can have a look [here](https://angular.io/docs/ts/latest/guide/setup.html#!#install-prerequisites).

```
git clone  https://:@gitlab.cern.ch:8443/lurodrig/products-ui.git products-ui
cd products-ui
npm start
```

## Contributors

For any comments, questions or suggestions, please do not hesitate in contact [me](http://profiles.web.cern.ch/720335).

## Reference

Some useful tutorials

 - https://www.barbarianmeetscoding.com/blog/2016/04/02/getting-started-with-angular-2-step-by-step-6-consuming-real-data-with-http/
 - https://coryrylan.com/blog/angular-form-builder-and-validation-management
 - http://learnangular2.com/forms/
