export class Product {
  id: number;
  name: string;
  description: string;
  arrival: Date;
  quantity: number;
}
