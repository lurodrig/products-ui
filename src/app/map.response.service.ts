import { Product } from './product';
import { Response } from '@angular/http';

export class MapResponseService {

    static mapProducts(response: Response): Product[] {
        console.log(JSON.stringify(response.json()));
        return response.json().map(MapResponseService.toProduct);
    }

    static mapProduct(response: Response): Product {
        return MapResponseService.toProduct(response.json());
    }

    static toProduct(r: any): Product {
        let product = <Product>({
            id: r.id,
            name: r.name,
            description: r.description,
            arrival: r.arrival,
            quantity: r.quantity,
        });
        console.log('Parsed product:', product);
        return product;
    }
}
