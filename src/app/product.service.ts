import { Injectable, Inject } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
import { Product } from './product';
import { APP_CONFIG, IAppConfig } from './app.config';
import { MapResponseService } from './map.response.service'

@Injectable()
export class ProductService {
  private productsUrl = this.config.apiEndpoint;
  private headers = new Headers({
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  }, );
  private options = new RequestOptions({ headers: this.headers });

  constructor(
    private http: Http,
    @Inject(APP_CONFIG) private config: IAppConfig) { }

  getProducts(): Observable<Product[]> {
    console.log(this.productsUrl);
    let products = this.http
      .get(`${this.productsUrl}`, { headers: this.headers })
      .map(MapResponseService.mapProducts)
      .catch(this.handleError);
    return products;
  }

  getProduct(id: number): Observable<Product> {
    console.log(this.productsUrl);
    let product = this.http
      .get(`${this.productsUrl}/${id}`, { headers: this.headers })
      .map(MapResponseService.mapProduct);
    return product;
  }

  update(product: Product): Observable<Product> {
    const url = `${this.productsUrl}/${product.id}`;
    return this.http
      .put(url, JSON.stringify(product), this.options)
      .catch(this.handleError);
  }

  create(product: Product): Observable<Product> {
    return this.http
      .post(this.productsUrl, JSON.stringify(product), this.options)
      .catch(this.handleError);
  }

  save(product: Product): Observable<Product> {
    console.log(JSON.stringify(product));
    if (product.id == undefined) {
      return this.http
        .post(this.productsUrl, JSON.stringify(product), this.options)
        .catch(this.handleError);
    } else {
      return this.http
        .post(this.productsUrl, JSON.stringify(product), this.options)
        .catch(this.handleError);
    }
  }

  delete(id: number): Promise<void> {
    const url = `${this.productsUrl}/${id}`;
    return this.http.delete(url, { headers: this.headers })
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  private handleError(error: any) {
    console.error('An error occurred', error);
    return Observable.throw(error.message || error);
  }
}