import { Injectable, Inject } from '@angular/core';
import 'rxjs/add/operator/switchMap';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Product } from './product';
import { ProductService } from './product.service';
import { APP_CONFIG, IAppConfig } from './app.config';

@Component({
  moduleId: module.id,
  selector: 'my-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  product: Product;
  sub: any;
  datePattern = this.config.datePattern;
  constructor(
    private productService: ProductService,
    private route: ActivatedRoute,
    private router: Router,
    @Inject(APP_CONFIG) private config: IAppConfig
  ) { }


  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      let idParam = params['id'];
      if (idParam == undefined) {
        this.product = new Product();
      } else {
        let id = Number.parseInt(params['id']);
        this.productService
          .getProduct(id)
          .subscribe(p => this.product = p);
      }
    });
  }

  goDashboard(): void {
    let link = ['/dashboard'];
    this.router.navigate(link);
  }

  save(): void {
    console.log(JSON.stringify(this.product));
    this.productService.save(this.product)
      .subscribe(() => this.goDashboard());
  }
}