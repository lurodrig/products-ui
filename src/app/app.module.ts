import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard.component';
import { ProductsComponent } from './products.component';
import { ProductDetailComponent } from './product-detail.component';
import { ProductService } from './product.service';
import { ProductSearchComponent } from './product-search.component'
import { APP_CONFIG, AppConfig } from './app.config';
import { Md2Module } from 'md2';
import { Ng2PaginationModule } from 'ng2-pagination';
import { MapResponseService } from './map.response.service';
import { MaterialModule } from '@angular/material';
import { ConfirmDialog } from './confirm.dialog.component';
import { DialogsService } from './dialogs.service';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    ReactiveFormsModule,
    Md2Module,
    Ng2PaginationModule,
    MaterialModule
  ],
  declarations: [
    AppComponent,
    DashboardComponent,
    ProductDetailComponent,
    ProductsComponent,
    ProductSearchComponent,
    ConfirmDialog
  ],
  entryComponents: [ConfirmDialog],
  providers: [
    ProductService,
    MapResponseService,
    DialogsService,
    { provide: APP_CONFIG, useValue: AppConfig }],
  bootstrap: [AppComponent]
})
export class AppModule { }
