import { Injectable, Inject } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Product } from './product';
import { APP_CONFIG, IAppConfig } from './app.config';
import { MapResponseService } from './map.response.service'

@Injectable()
export class ProductSearchService {

    private headers = new Headers({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }, );

    constructor(
        private http: Http,
        @Inject(APP_CONFIG) private config: IAppConfig) { }

    search(term: string): Observable<Product[]> {
        let searchUrl = this.config.apiEndpoint
            + '/search?term='
            + term;
        let products = this.http
            .get(`${searchUrl}`, { headers: this.headers })
            .map(MapResponseService.mapProducts)
            .catch(this.handleError);
        return products;
    }
    private handleError(error: any) {
        console.error('An error occurred', error);
        return Observable.throw(error.message || error);
    }
}
