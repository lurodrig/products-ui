import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from './product';
import { ProductService } from './product.service';
import { MdDialog } from '@angular/material';
import { DialogsService } from './dialogs.service';

@Component({
  moduleId: module.id,
  selector: 'my-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products: Product[];
  selectedProduct: Product;
  public result: any;

  constructor(
    private router: Router,
    private productService: ProductService,
    private dialogsService: DialogsService,
    private viewContainerRef: ViewContainerRef) { }

  getProducts(): void {
    this.productService.getProducts().subscribe(products => this.products = products);
  }

  ngOnInit(): void {
    this.getProducts();
  }

  onSelect(product: Product): void {
    this.selectedProduct = product;
  }

  gotoDetail(): void {
    this.router.navigate(['/detail', this.selectedProduct.id]);
  }

  delete(product: Product) {
    console.log('deleting ' + product.id + '!!!');
    this.dialogsService
      .confirm('Confirm Dialog', 'Are you sure you want to do delete ' + product.name +'?', this.viewContainerRef)
      .subscribe(res => {
        if (res == true) {
          this.productService
            .delete(product.id)
            .then(() => {
              this.products = this.products.filter(h => h !== product);
              if (this.selectedProduct === product) { this.selectedProduct = null; }
            });
        } else {
          console.log(product.id + ' delete dialog: ' + res);
        }
      });
  }
}